package storage

import (
	"context"

	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Role() RoleRepoI
	RolePermission() RolePermissionRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *auth_service.CreateUser) (resp *auth_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *models.UserPrimaryKeyWithLogin) (resp *auth_service.User, err error)
}

type RolePermissionRepoI interface {
	GetByRoleId(ctx context.Context, id string) (resp []*auth_service.PermissionWithAction, err error)
}

type RoleRepoI interface {
	GetByID(ctx context.Context, req *auth_service.RolePrimaryKey) (resp *auth_service.Role, err error)
}
