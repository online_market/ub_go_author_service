package postgres

import (
	"context"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4/pgxpool"

	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/storage"
)

type RolePermissionRepo struct {
	db *pgxpool.Pool
}

func NewRolePermissionRepo(db *pgxpool.Pool) storage.RolePermissionRepoI {
	return &RolePermissionRepo{
		db: db,
	}
}

func (c *RolePermissionRepo) GetByRoleId(ctx context.Context, id string) (resp []*auth_service.PermissionWithAction, err error) {

	var (
		obj   pgtype.JSONB
		query string
	)

	resp = []*auth_service.PermissionWithAction{}

	query = `
		SELECT
			JSON_AGG (
				JSONB_BUILD_OBJECT (
				'id', p.id,
				'name', p.name,
				'action', rp.action
				)
			)
		FROM
			role_permission AS rp
		LEFT JOIN permission AS p ON p.id = rp.permission_id
		WHERE rp.role_id = $1
	`

	err = c.db.QueryRow(ctx, query, id).Scan(&obj)
	if err != nil {
		return nil, err
	}

	err = obj.AssignTo(&resp)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
