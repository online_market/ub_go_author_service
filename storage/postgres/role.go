package postgres

import (
	"context"
	"database/sql"

	"github.com/jackc/pgx/v4/pgxpool"

	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/storage"
)

type RoleRepo struct {
	db *pgxpool.Pool
}

func NewRoleRepo(db *pgxpool.Pool) storage.RoleRepoI {
	return &RoleRepo{
		db: db,
	}
}

func (c *RoleRepo) GetByID(ctx context.Context, req *auth_service.RolePrimaryKey) (resp *auth_service.Role, err error) {

	query := `
		SELECT
			id,
			name,
			created_at,
			updated_at
		FROM
			role
		WHERE id = $1
	`

	var (
		id         sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).
		Scan(
			&id,
			&name,
			&created_at,
			&updated_at,
		)
	if err != nil {
		return nil, err
	}

	return &auth_service.Role{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}
