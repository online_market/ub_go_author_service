package service

import (
	"context"
	"errors"

	"online_market/ub_go_author_sevice/config"
	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/grpc/client"
	"online_market/ub_go_author_sevice/models"
	"online_market/ub_go_author_sevice/pkg/helper"
	"online_market/ub_go_author_sevice/pkg/logger"
	"online_market/ub_go_author_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SessionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*auth_service.UnimplementedSessionServiceServer
}

func NewSessionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *SessionService {
	return &SessionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *SessionService) Login(ctx context.Context, req *auth_service.LoginRequest) (resp *auth_service.LoginReponse, err error) {

	i.log.Info("---Login------>", logger.Any("req", req))

	if len(req.Login) < 6 {
		i.log.Error("!!!Login--->", logger.Error(errors.New("invalid login")))
		return nil, status.Error(codes.InvalidArgument, errors.New("invalid login").Error())
	}

	if len(req.Password) < 6 {
		i.log.Error("!!!Login--->", logger.Error(errors.New("invalid password")))
		return nil, status.Error(codes.InvalidArgument, errors.New("invalid password").Error())
	}

	user, err := i.strg.User().GetByPKey(ctx, &models.UserPrimaryKeyWithLogin{Login: req.Login})
	if err != nil {
		i.log.Error("!!!Login->GetUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if user.Password != req.Password {
		i.log.Error("!!!Login--->", logger.Error(errors.New("invalid login or password")))
		return nil, status.Error(codes.InvalidArgument, errors.New("invalid login or password").Error())
	}

	resp = &auth_service.LoginReponse{}
	resp.UserFound = true
	resp.User = user

	permissions, err := i.strg.RolePermission().GetByRoleId(ctx, user.RoleId)
	if err != nil {
		i.log.Error("!!!Login->GetByRoleId--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp.Permissions = permissions

	role, err := i.strg.Role().GetByID(ctx, &auth_service.RolePrimaryKey{Id: user.RoleId})
	if err != nil {
		i.log.Error("!!!Login->Role->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp.Role = role

	data := map[string]interface{}{
		"id":      user.Id,
		"role_id": user.RoleId,
	}

	token, err := helper.GenerateJWT(data, config.TokenExpireTime, i.cfg.SecretKey)
	if err != nil {
		i.log.Error("!!!Login->GenerateJWT--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp.AccessToken = token

	return
}

func (i *SessionService) HasAccess(ctx context.Context, req *auth_service.HasAccessRequest) (resp *auth_service.HasAccessResponse, err error) {

	tokenInfo, err := helper.ParseClaims(req.AccessToken, i.cfg.SecretKey)
	if err != nil {
		i.log.Error("!!!HasAccess->ParseClaims--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &auth_service.HasAccessResponse{
		Id:     tokenInfo.UserID,
		RoleId: tokenInfo.RoleID,
	}, nil
}
