package service

import (
	"context"
	"errors"
	"online_market/ub_go_author_sevice/config"
	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/grpc/client"
	"online_market/ub_go_author_sevice/models"
	"online_market/ub_go_author_sevice/pkg/logger"
	"online_market/ub_go_author_sevice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AuthService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*auth_service.UnimplementedAuthServiceServer
}

func NewAuthService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *AuthService {
	return &AuthService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *AuthService) Create(ctx context.Context, req *auth_service.CreateUser) (resp *auth_service.User, err error) {

	i.log.Info("---CreateAuth------>", logger.Any("req", req))

	if len(req.Login) < 6 {
		i.log.Error("!!!CreateAuth->Auth", logger.Error(errors.New("invalid login")))
		return nil, status.Error(codes.InvalidArgument, errors.New("invalid login").Error())
	}

	if len(req.Password) < 6 {
		i.log.Error("!!!CreateAuth->Auth", logger.Error(errors.New("invalid password")))
		return nil, status.Error(codes.InvalidArgument, errors.New("invalid password").Error())
	}

	pKey, err := i.strg.User().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateAuth->Auth->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.User().GetByPKey(ctx, &models.UserPrimaryKeyWithLogin{Id: pKey.Id})
	if err != nil {
		i.log.Error("!!!GetByPKeyAuth->Auth->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *AuthService) GetByID(ctx context.Context, req *auth_service.UserPrimaryKey) (resp *auth_service.User, err error) {

	i.log.Info("---GetAuthByID------>", logger.Any("req", req))

	resp, err = i.strg.User().GetByPKey(ctx, &models.UserPrimaryKeyWithLogin{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetAuthByID->Auth->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

// func (i *AuthService) GetList(ctx context.Context, req *auth_service.GetListAuthRequest) (resp *auth_service.GetListAuthResponse, err error) {

// 	i.log.Info("---GetAuths------>", logger.Any("req", req))

// 	resp, err = i.strg.Auth().GetAll(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!GetAuths->Auth->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return
// }

// func (i *AuthService) Update(ctx context.Context, req *auth_service.UpdateAuth) (resp *auth_service.Auth, err error) {

// 	i.log.Info("---UpdateAuth------>", logger.Any("req", req))

// 	rowsAffected, err := i.strg.Auth().Update(ctx, req)

// 	if err != nil {
// 		i.log.Error("!!!UpdateAuth--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Auth().GetByPKey(ctx, &auth_service.AuthPrimaryKey{Id: req.Id})
// 	if err != nil {
// 		i.log.Error("!!!GetAuth->Auth->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }

// func (i *AuthService) UpdatePatch(ctx context.Context, req *auth_service.UpdatePatchAuth) (resp *auth_service.Auth, err error) {

// 	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

// 	updatePatchModel := models.UpdatePatchRequest{
// 		Id:     req.GetId(),
// 		Fields: req.GetFields().AsMap(),
// 	}

// 	rowsAffected, err := i.strg.Auth().UpdatePatch(ctx, &updatePatchModel)

// 	if err != nil {
// 		i.log.Error("!!!UpdatePatchAuth--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if rowsAffected <= 0 {
// 		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
// 	}

// 	resp, err = i.strg.Auth().GetByPKey(ctx, &auth_service.AuthPrimaryKey{Id: req.Id})
// 	if err != nil {
// 		i.log.Error("!!!GetAuth->Auth->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.NotFound, err.Error())
// 	}

// 	return resp, err
// }

// func (i *AuthService) Delete(ctx context.Context, req *auth_service.AuthPrimaryKey) (resp *empty.Empty, err error) {

// 	i.log.Info("---DeleteAuth------>", logger.Any("req", req))

// 	err = i.strg.Auth().Delete(ctx, req)
// 	if err != nil {
// 		i.log.Error("!!!DeleteAuth->Auth->Get--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return &empty.Empty{}, nil
// }
