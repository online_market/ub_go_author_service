package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"online_market/ub_go_author_sevice/config"
	"online_market/ub_go_author_sevice/genproto/auth_service"
	"online_market/ub_go_author_sevice/grpc/client"
	"online_market/ub_go_author_sevice/grpc/service"
	"online_market/ub_go_author_sevice/pkg/logger"
	"online_market/ub_go_author_sevice/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	auth_service.RegisterAuthServiceServer(grpcServer, service.NewAuthService(cfg, log, strg, srvc))
	auth_service.RegisterSessionServiceServer(grpcServer, service.NewSessionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
