

SELECT
    JSON_AGG (
        JSONB_BUILD_OBJECT (
        'id', p.id,
        'name', p.name,
        'action', rp.action
        )
    )
FROM
    role_permission AS rp
LEFT JOIN permission AS p ON p.id = rp.permission_id
WHERE rp.role_id = 'cdae9800-b3c7-429d-ad94-d99d72be92c1'
